import 'package:flutter/material.dart';

import 'profile.dart';
import 'result.dart';

class ProfileCell extends StatelessWidget {
  final String title;

  final bool isHeader;

  const ProfileCell({
    Key? key,
    required this.title,
    
    this.isHeader = false,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Align(
      alignment: isHeader ? Alignment.centerLeft : Alignment.centerLeft,
      child: Padding(
        padding: const EdgeInsets.all(5.0),
        child: Text(title,
            style: TextStyle(
                fontWeight: isHeader ? FontWeight.bold : FontWeight.normal)),
      ),
    );
  }
}
