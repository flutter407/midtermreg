import 'package:flutter/material.dart';
import 'package:syncfusion_flutter_gauges/gauges.dart';
import 'profile_cell.dart';
import 'profile.dart';
import 'result.dart';
import 'package:at_gauges/at_gauges.dart';

class _ChartData {
  _ChartData(this.x, this.y, this.color);
  final String x;
  final double y;
  final Color color;
}

class ProfileTable extends StatelessWidget {
  const ProfileTable({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    // late TooltipBehavior _tooltip;
    final List<_ChartData> chartData = [
      _ChartData('David', 87.75, Color.fromARGB(255, 253, 216, 53)),
      _ChartData('David', 22.25, Color.fromARGB(255, 188, 188, 202)),
    ];
    return ListView(padding: EdgeInsets.fromLTRB(0, 5, 0, 0), children: [
      SingleChildScrollView(
        scrollDirection: Axis.vertical,
        child: Column(
          children: [
            Row(children: <Widget>[
              Expanded(
                flex: 1, // 20%
                child: Container(color: Colors.red),
              ),
              Expanded(
                flex: 8, // 60%
                child: Column(
                  children: [
                    Container(
                        child: Text("ประวัตินิสิต",
                            style: TextStyle(fontSize: 30))),
                    Container(
                      width: 300,
                      height: 300,
                      decoration: BoxDecoration(
                        image: DecorationImage(
                          // fit: BoxFit.fill,
                          image: AssetImage("assets/images/Coolguy.png"),
                        ),
                      ),
                    ),
                    SizedBox(
                      height: 30,
                    ),
                    Table(
                      columnWidths: const <int, TableColumnWidth>{
                        0: FixedColumnWidth(150),
                        1: FixedColumnWidth(350)
                      },
                      defaultVerticalAlignment:
                          TableCellVerticalAlignment.middle,
                      children: <TableRow>[
                        TableRow(
                          decoration: BoxDecoration(
                            color: Color.fromARGB(255, 134, 124, 124),
                          ),
                          children: const <Widget>[
                            ProfileCell(
                                title: 'ข้อมูลส่วนบุคคล', isHeader: true),
                            ProfileCell(title: '', isHeader: true),
                          ],
                        ),
                        ...Profile.getProfiles().map((employee) {
                          return TableRow(children: [
                            ProfileCell(title: employee.name),
                            ProfileCell(title: employee.role),
                          ]);
                        }),
                      ],
                    ),
                    SizedBox(
                      height: 30,
                    ),
                    Table(
                      columnWidths: const <int, TableColumnWidth>{
                        0: FixedColumnWidth(150),
                        1: FixedColumnWidth(350)
                      },
                      defaultVerticalAlignment:
                          TableCellVerticalAlignment.middle,
                      children: <TableRow>[
                        TableRow(
                          decoration: BoxDecoration(
                            color: Color.fromARGB(255, 134, 124, 124),
                          ),
                          children: const <Widget>[
                            ProfileCell(title: 'ผลการศึกษา', isHeader: true),
                            ProfileCell(title: '', isHeader: true),
                          ],
                        ),
                        ...Result.getResults().map((employee) {
                          return TableRow(children: [
                            ProfileCell(title: employee.name),
                            ProfileCell(title: employee.role),
                          ]);
                        }),
                      ],
                    ),
                    SizedBox(
                      height: 30,
                    ),
                    SimpleRadialGauge(
                      maxValue: 4.00,
                      actualValue: 3.51,
                      // Optional Parameters
                      minValue: 0.00,
                      size: 300,
                      title: Text('คะแนนเฉลี่ยสะสม'),
                      titlePosition: TitlePosition.top,
                      pointerColor: Color.fromARGB(255, 245, 225, 50),
                      // needleColor: Color.fromARGB(255, 243, 33, 33),
                      decimalPlaces: 2,
                      isAnimate: true,
                      animationDuration: 2000,
                      unit: 'GPA',
                    ),
                    SizedBox(height: 30,),
                    Container(
                      height: 500,
                      width: 500,
                      child: SimpleLinearGauge(
                        maxValue: 132,
                        actualValue: 96,
                        //Optional Parameters
                        minValue: 0,
                        divisions: 10,
                        title: const Text('หน่วยกิตที่ผ่านแล้ว',style: TextStyle(fontSize: 25),),
                        titlePosition: TitlePosition.top,
                        pointerColor: Color.fromARGB(255, 245, 225, 50),
                        pointerIcon:
                            const Icon(Icons.thumb_up_alt_rounded, color: Color.fromARGB(255, 195, 255, 57)),
                        decimalPlaces: 0,
                        isAnimate: true,
                        animationDuration: 5000,
                        gaugeOrientation: GaugeOrientation.vertical,
                        gaugeStrokeWidth: 5,
                        rangeStrokeWidth: 5,
                        majorTickStrokeWidth: 3,
                        minorTickStrokeWidth: 3,
                        actualValueTextStyle:
                            const TextStyle(color: Colors.black, fontSize: 15),
                        majorTickValueTextStyle:
                            const TextStyle(color: Colors.black),
                      ),
                    )
                  ],
                ),
              ),
              Expanded(
                flex: 1, // 20%
                child: Container(color: Colors.blue),
              )
            ]),
          ],
        ),
      ),
    ]);

    // return Table(
    //   columnWidths: const <int, TableColumnWidth>{2: FixedColumnWidth(100)},
    //   defaultVerticalAlignment: TableCellVerticalAlignment.middle,
    //   children: <TableRow>[
    //     TableRow(
    //       decoration: BoxDecoration(
    //         color: Colors.green[200],
    //       ),
    //       children: const <Widget>[
    //         ProfileCell(title: 'Name', isHeader: true),
    //         ProfileCell(title: 'Role', isHeader: true),
    //         ProfileCell(title: 'Hourly rate', isHeader: true),
    //       ],
    //     ),
    //     ...Profile.getEmployees().map((employee) {
    //       return TableRow(children: [
    //         ProfileCell(title: employee.name),
    //         ProfileCell(title: employee.role),
    //         ProfileCell(title: '\$ ${employee.hourlyRate}'),
    //       ]);
    //     }),
    //   ],
    // );
  }
}
