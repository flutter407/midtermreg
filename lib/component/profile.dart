class Profile {
  final String name;
  final String role;

  Profile(this.name, this.role);

  static List<Profile> getProfiles() {
    return [
      Profile('ชื่อ:','นายอัษฎาวุฒิ ทัดอยู่'),
      Profile('รหัสนิสิต:', '63160277'),
      Profile(
          'เลขที่บัตรประชาชน:', '1160101768017'),
      Profile('ชื่ออังกฤษ:', 'MR. ADSADAWUT TADYOO'),
      Profile('คณะ:', 'คณะวิทยาการสารสนเทศ '),
      Profile('สาขา:', 'วิทยาการคอมพิวเตอร์'),
      Profile('วิทยาเขต:', 'บางแสน')
    ];
  }

  static List<Profile> getResult(){
    return[
      Profile('ผลการศึกษา:','นายอัษฎาวุฒิ ทัดอยู่'),
    ];
  }
}
