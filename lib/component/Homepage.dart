import 'package:flutter/material.dart';
import 'package:weather_icons/weather_icons.dart';
import 'package:device_preview/device_preview.dart';
import 'package:marquee/marquee.dart';
import 'package:simple_grid/simple_grid.dart';
// import 'package:flutter_circular_chart/flutter_circular_chart.dart';

import '../main.dart';
import 'profile_table.dart';

double Blankspace = 1400.0;
int pageIndex = 0;

class MainHome extends StatefulWidget {
  const MainHome({super.key});
  @override
  State<MainHome> createState() => _MainHomeState();
}

class _MainHomeState extends State<MainHome> {
  
  @override
  Widget build(BuildContext context) {
    double width1 = MediaQuery.of(context).size.width;
    List<Widget> pageList = <Widget>[buildBody(context),Login(),ProfileTable()];
    
    return DevicePreview(
      tools: const [
        DeviceSection(),
      ],
      builder: (context) => MaterialApp(
        
        debugShowCheckedModeBanner: false,
        useInheritedMediaQuery: true,
        builder: DevicePreview.appBuilder,
        locale: DevicePreview.locale(context),
        title: 'my Reg',
        theme: ThemeData(
          primarySwatch: Colors.grey,
        ),
        
        home: SafeArea(
    child: Container(
      child: Scaffold(
        appBar: buildAppBar(),
        body: pageList[swithPage(pageIndex)] == pageList[0]? buildBody(context): pageList[swithPage(pageIndex)],
        drawer: Container(
          width: 250,
          child: Drawer(
            
            // Add a ListView to the drawer. This ensures the user can scroll
            // through the options in the drawer if there isn't enough vertical

            // space to fit everything.
            child: ListView(
              // Important: Remove any padding from the ListView.
              padding: EdgeInsets.zero,
              children: [
                const UserAccountsDrawerHeader(
                  margin: EdgeInsets.all(0),
                  decoration:
                      BoxDecoration(color: Color.fromARGB(255, 253, 216, 53)),
                  accountEmail: Text(
                    "63160277@go.buu.ac.th",
                    style: TextStyle(fontWeight: FontWeight.w900),
                  ),
                  accountName: Text("Adsadawut Tadyoo",
                      style: TextStyle(fontWeight: FontWeight.bold)),
                  currentAccountPicture: CircleAvatar(
                      foregroundImage:
                          AssetImage('assets/images/Coolguy.png')),
                ),
                Container(
                  child: ListTile(
                    leading: Icon(Icons.home),
                    title: const Text('หน้าแรก'),
                    onTap: () {
                      setState(() {
                        swithPage(0);
                      });
                      // Navigator.pop(context);
                      
                      // Update the state of the app
                      // ...
                      // Then close the drawer
                    },
                  ),
                ),
                Container(
                  child: ListTile(
                    leading: Icon(Icons.person_outline_sharp),
                    title: const Text('ประวัตินิสิต'),
                    onTap: () { 
                      setState(() {
                        swithPage(2);
                      });
                      // Navigator.pop(context);
                    },
                  ),
                ),
                Container(
                  child: ListTile(
                    leading: Icon(Icons.my_library_books_rounded),
                    title: const Text('ตารางเรียน'),
                    onTap: () {
                      // Update the state of the app
                      // ...
                      // Then close the drawer
                      pageIndex=0;
                      // Navigator.pop(context);
                    },
                  ),
                ),
                Container(
                  child: ListTile(
                    leading: Icon(Icons.attach_money_sharp),
                    title: const Text('ชำระหนี้'),
                    onTap: () {
                      // Update the state of the app
                      // ...
                      // Then close the drawer
                      pageIndex=0;
                      // Navigator.pop(context);
                    },
                  ),
                ),
                Container(
                  child: ListTile(
                    leading: Icon(Icons.logout),
                    title: const Text('ออกจากระบบ'),
                    onTap: () {
                      // Update the state of the app
                      // ...
                      // Then close the drawer
                      pageIndex=0;
                      Navigator.pop(context);
                    },
                  ),
                ),
                
              ],
            ),
          ),
        ),
      ),
    ),
  ),
      ),
    );
  }
}

int swithPage(int page){
  pageIndex=page;
  return pageIndex;
}


// Widget HomePage(BuildContext context) {
  
  
//   return 
// }

AppBar buildAppBar() {
  return AppBar(
    leading: Builder(
      builder: (BuildContext context) {
        return IconButton(
          icon: const Icon(
            Icons.menu,
            color: Color.fromARGB(255, 0, 0, 0),
            size: 35, // Changing Drawer Icon Size
          ),
          onPressed: () {
            Scaffold.of(context).openDrawer();
          },
          tooltip: MaterialLocalizations.of(context).openAppDrawerTooltip,
        );
      },
    ),
    title: Transform.translate(
      offset: Offset(-24.0, 0.0),
      child: Container(
        child: Row(children: [
          Center(
            child: Container(
              margin: const EdgeInsets.fromLTRB(10, 0, 10, 0),
              width: 50,
              height: 50,
              decoration: BoxDecoration(
                image: DecorationImage(
                  fit: BoxFit.fill,
                  image: AssetImage("assets/images/buu_logo_eng.png"),
                ),
              ),
            ),
          ),
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                "มหาวิทยาลัยบูรพา",
                style: TextStyle(fontSize: 18, color: Colors.black),
              ),
              Text(
                "Burapha University",
                style: TextStyle(fontSize: 15, color: Colors.black),
              ),
            ],
          )
        ]),
      ),
    ),
    backgroundColor: Colors.yellow[600],
    elevation: 0.0,
  );
}

Widget buildBody(BuildContext context) {
  return Column(
    children: [
      Row(
        children: [
          Expanded(
              child: Container(
                  height: 50,
                  padding: EdgeInsets.fromLTRB(0, 0, 0, 0),
                  color: Colors.grey,
                  child: Column(
                    children: [
                      Container(
                        height: 50,
                        padding: EdgeInsets.fromLTRB(0, 8, 0, 0),
                        child: buildTextSlide(
                            "ยินดีต้อนรับเข้าสู่ระบบบริการการศึกษา",
                            context),
                      ),
                    ],
                  ))),
          // SpGridItem(child: child)
        ],
      ),
      Expanded(
        child: ListView(padding: EdgeInsets.all(5), children: [
          SingleChildScrollView(
              scrollDirection: Axis.vertical,
              child: Column(
                children: [
                  SpGrid(width: MediaQuery.of(context).size.width, children: [
                    SpGridItem(
                      xs: 12,
                      sm: 12,
                      md: 12,
                      lg: 12,
                      child: Center(
                        child: Text(
                          "ประกาศ/ข่าวสาร",
                          style: TextStyle(fontSize: 30),
                        ),
                      ),
                    ),
                    SpGridItem(
                        xs: 12, sm: 6, md: 6, lg: 6, child: buildCard()),
                    SpGridItem(
                        xs: 12, sm: 6, md: 6, lg: 6, child: buildCard2()),
                    SpGridItem(
                        xs: 12, sm: 6, md: 6, lg: 6, child: buildCard3()),
                  ]),
                ],
              )),
        ]),
      ),
    ],
  );
}

Widget buildTextSlide(String text, BuildContext context) {
  return Marquee(
    text: text,
    style: TextStyle(fontWeight: FontWeight.bold, fontSize: 20),
    scrollAxis: Axis.horizontal,
    crossAxisAlignment: CrossAxisAlignment.start,
    blankSpace: setBlankSpace(Blankspace, context),
    // velocity: 100.0,
    pauseAfterRound: Duration(microseconds: 0),
    showFadingOnlyWhenScrolling: true,
    fadingEdgeStartFraction: 0,
    fadingEdgeEndFraction: 0,
    // numberOfRounds: 3,
    // startPadding: 100.0,
    accelerationDuration: Duration(seconds: 0),
    // accelerationCurve: Curves.linear,
    decelerationDuration: Duration(milliseconds: 1),
    // decelerationCurve: Curves.easeOut,
  );
}

double setBlankSpace(Blankspace, BuildContext context) {
  double width1 = MediaQuery.of(context).size.width;
  double height1 = MediaQuery.of(context).size.height;
  if (width1 < 450.0) {
    Blankspace = 270.0;
    return Blankspace;
  } else if (width1 > 450.0 && width1 < 1000.0) {
    Blankspace = 800.0;
    return Blankspace;
  }

  return Blankspace;
}

Card buildCard() {
  var heading = 'เปิดรับสมัครนิสิตเข้าหอพัก ภาคต้น 2565';
  var subheading = 'สำหรับนิสิตใหม่ ปี 1 TCAS รอบ 3';
  var cardImage = AssetImage('assets/images/domitory.png');
  var supportingText =
      'จองหอหักผ่านระบบรายงานตัวนิสิตออนไลน์ และชำระค่าหอพัก วันที่ 7-9 มิ.ย. 2565';
  return Card(
      elevation: 5.0,
      child: Column(
        children: [
          ListTile(
            title: Text(heading, style: TextStyle(fontSize: 20)),
            subtitle: Text(subheading, style: TextStyle(fontSize: 16)),
            //  trailing: Icon(Icons.favorite_outline),
          ),
          Container(
            child: AspectRatio(
              aspectRatio: 2 / 1,
              child: Container(
                width: 80,
                height: 10,
                // color: Colors.teal,
                child: Ink.image(
                  image: cardImage,
                  fit: BoxFit.contain,
                ),
              ),
            ),
          ),
          Container(
            padding: EdgeInsets.all(16.0),
            alignment: Alignment.centerLeft,
            child: Text(supportingText),
          ),
          ButtonBar(
            children: [
              TextButton(
                child: const Text('เพิ่มเติม',
                    style: TextStyle(
                        color: Colors.black, fontWeight: FontWeight.bold)),
                onPressed: () {/* ... */},
              )
            ],
          )
        ],
      ));
}

Card buildCard2() {
  var heading = 'การยื่นคำร้องขอสำเร็จการศึกษา ภาคปลาย ปีการศึกษา 2565(ด่วน)';
  var subheading =
      'นิสิตสิตจะรับเอกสารจบการศึกษาได้ต่อเมื่อนิสิตมีสถานะจบการศึกษา';
  var cardImage = AssetImage('assets/images/grad652.png');
  var supportingText =
      'นิสิตต้องยื่นคำร้องขอสำเร็จการศึกษาและชำระเงินตามกำหนดเวลาหากนิสิตไม่ได้ยื่นสำเร็จการศึกษาทางกองทะบียนฯจะไม่เสนอชื่อสำเร็จการศึกษา';
  return Card(
      elevation: 5.0,
      child: Column(
        children: [
          ListTile(
            title: Text(heading, style: TextStyle(fontSize: 20)),
            subtitle: Text(subheading, style: TextStyle(fontSize: 16)),
            //  trailing: Icon(Icons.favorite_outline),
          ),
          Container(
            child: AspectRatio(
              aspectRatio: 2 / 1,
              child: Container(
                width: 80,
                height: 10,
                // color: Colors.teal,
                child: Ink.image(
                  image: cardImage,
                  fit: BoxFit.contain,
                ),
              ),
            ),
          ),
          Container(
            padding: EdgeInsets.all(16.0),
            alignment: Alignment.centerLeft,
            child: Text(supportingText,style: TextStyle(
                        color: Colors.black, fontSize: 12),),
          ),
          ButtonBar(
            children: [
              TextButton(
                child: const Text('เพิ่มเติม',
                    style: TextStyle(
                        color: Colors.black, fontWeight: FontWeight.bold)),
                onPressed: () {/* ... */},
              )
            ],
          )
        ],
      ));
}

Card buildCard3() {
  var heading = 'แบบประเมินความคิดเห็นต่อการให้บริการของสำนักงานอธิการบดี';
  var subheading =
      'ขอเชิญนิสิตและผู้ปกครองร่วมทำแบบประเมินความคิดเห็นต่อการให้บริการของสำนักงานอธิการบดี';
  var cardImage = AssetImage('assets/images/assesment.jpg');
  return Card(
      elevation: 5.0,
      child: Column(
        children: [
          ListTile(
            title: Text(heading, style: TextStyle(fontSize: 20)),
            subtitle: Text(subheading, style: TextStyle(fontSize: 16)),
            //  trailing: Icon(Icons.favorite_outline),
          ),
          Container(
            child: AspectRatio(
              aspectRatio: 2 / 1,
              child: Container(
                width: 80,
                height: 10,
                // color: Colors.teal,
                child: Ink.image(
                  image: cardImage,
                  fit: BoxFit.contain,
                ),
              ),
            ),
          ),
          ButtonBar(
            children: [
              TextButton(
                child: const Text('สำหรับนิสิต',
                    style: TextStyle(
                        color: Colors.black, fontWeight: FontWeight.bold)),
                onPressed: () {/* ... */},
              ),
              TextButton(
                child: const Text('สำหรับผู้ปกครอง',
                    style: TextStyle(
                        color: Colors.black, fontWeight: FontWeight.bold)),
                onPressed: () {/* ... */},
              )
            ],
          )
        ],
      ));
}
