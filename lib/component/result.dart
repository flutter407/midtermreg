class Result {
  final String name;
  final String role;

  Result(this.name, this.role);

  static List<Result> getResults() {
    return [
      Result('หน่วยกิตคำนวณ:','96'),
      Result('หน่วยกิตที่ผ่าน:', '96'),
      Result('คะแนนเฉลี่ยสะสม:', '3.51'),
    ];
  }

}
