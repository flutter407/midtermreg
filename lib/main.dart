import 'package:flutter/material.dart';
import 'package:weather_icons/weather_icons.dart';
import 'package:device_preview/device_preview.dart';
import 'package:marquee/marquee.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:simple_grid/simple_grid.dart';

import 'component/Homepage.dart';
// import 'HomePage.dart';

void main() {
  runApp(const MaterialApp(
    title: 'Navigation Basics',
    home: Login(),
  ));
}

class Login extends StatefulWidget {
  const Login({super.key});

  @override
  State<Login> createState() => _LoginState();
}

class _LoginState extends State<Login> {
  
  @override
  Widget build(BuildContext context) {
   
    return DevicePreview(
      tools: const [
        DeviceSection(),
      ],
      builder: (context) => MaterialApp(
        debugShowCheckedModeBanner: false,
        useInheritedMediaQuery: true,
        builder: DevicePreview.appBuilder,
        locale: DevicePreview.locale(context),
        title: 'my REG',
        theme: ThemeData(
          primarySwatch: Colors.grey,
        ),
        home: buildSecond(context),
      ),
    );
  }
}



@override
Widget buildSecond(BuildContext context) {
   
  TextEditingController username = TextEditingController();
  TextEditingController password = TextEditingController();
  return SafeArea(
    child: Scaffold(
      appBar: buildAppBar(),
      body: ListView(
        padding: const EdgeInsets.fromLTRB(20,10,20,20),
        children: [
          SpGrid(width: MediaQuery.of(context).size.width, children: [
            SpGridItem(
              xs: 12,
              sm: 12,
              md: 12,
              lg: 12,
              child: SizedBox(
                height: 15,
              ),
            ),
            SpGridItem(
                xs: 12,
                sm: 12,
                md: 12,
                lg: 12,
                child: Text(
                  'เข้าสู่ระบบบริการการศึกษา',
                  style: const TextStyle(
                      fontWeight: FontWeight.bold,
                      color: Colors.blueGrey,
                      fontSize: 20),
                  textAlign: TextAlign.center,
                )),
            SpGridItem(
              xs: 12,
              sm: 12,
              md: 4,
              lg: 12,
              child: SizedBox(
                height: 15,
              ),
            ),
            SpGridItem(
              xs: 12,
              sm: 12,
              md: 12,
              lg: 12,
              child: Container(
                width: 200,
                height: 200,
                decoration: BoxDecoration(
                  image: DecorationImage(
                    image: AssetImage("assets/images/Buu-logo.png"),
                  ),
                ),
              ),
            ),
            SpGridItem(
              xs: 12,
              sm: 12,
              md: 12,
              lg: 12,
              child: SizedBox(
                height: 30,
              ),
            ),
             SpGridItem(
              xs: 12,
              sm: 3,
              md: 3,
              lg: 3,
              child: SizedBox(),
            ),
            SpGridItem(
              xs: 12,
              sm: 6,
              md: 6,
              lg: 6,
              child: 
              TextField(
                  controller: username,
                  decoration: InputDecoration(
                      labelText: 'UserName', border: OutlineInputBorder())),
            ),
            SpGridItem(
              xs: 12,
              sm: 12,
              md: 12,
              lg: 12,
              child: SizedBox(
                height: 15,
              ),
            ),
            SpGridItem(
              xs: 12,
              sm: 3,
              md: 3,
              lg: 3,
              child: SizedBox(),
            ),
            SpGridItem(
              xs: 12,
              sm: 6,
              md: 6,
              lg: 6,
              child: TextField(
                  controller: password,
                  obscureText: true,
                  decoration: InputDecoration(
                      labelText: 'Password', border: OutlineInputBorder())),
            ),
            SpGridItem(
              xs: 12,
              sm: 12,
              md: 12,
              lg: 12,
              child: SizedBox(
                height: 50,
              ),
            ),
            SpGridItem(
              xs: 12,
              sm: 4,
              md: 4,
              lg: 4,
              child: SizedBox(),
            ),
            SpGridItem(
              xs: 12,
              sm: 4,
              md: 4,
              lg: 4,
              child: Container(
                child: ElevatedButton(
                  onPressed: () {
                    //               if (username.text == "admin" && password.text == "1234") {
                    //                 print("USERNAME = ${username.text}, PASSWORD = ${password.text}");
                    //                 // setState(() {
                    //                 //   username.text = 'admin';
                    //                 //   password.text = '1234';

                    //                 //   setUsername(username.text);
                    //                 //   setPassword(password.text);
                    //                 //   setStatus("Login Success");
                    //                 // });
                    //               } else {
                    //                 print("USER = other");
                    //  /*                   setUsername("failed");
                    //                 setPassword("failed"); */
                    //                 setStatus("Login Failed");
                    //               }
                    //             },
                    Navigator.push(
                      context,
                      MaterialPageRoute(builder: (context) => const MainHome()),
                    );
                  },
                  child: Text("เข้าสู่ระบบ"),
                  style: ButtonStyle(
                      backgroundColor:
                          MaterialStateProperty.all(Colors.yellow[600]),
                      padding: MaterialStateProperty.all(
                          EdgeInsets.fromLTRB(30, 20, 30, 20)),
                      textStyle:
                          MaterialStateProperty.all(TextStyle(fontSize: 30))),
                ),
              ),
            ),
            
          ]),
          // ElevatedButton(
          //   onPressed: () {
          //     Navigator.push(context,
          //         MaterialPageRoute(builder: (context) => MyProfilePage()));
          //   },
          //   child: Text("ไปหน้า Profile"),
          //   style: ButtonStyle(
          //       backgroundColor: MaterialStateProperty.all(Colors.blueGrey),
          //       padding: MaterialStateProperty.all(
          //           EdgeInsets.fromLTRB(50, 20, 50, 20)),
          //       textStyle:
          //           MaterialStateProperty.all(TextStyle(fontSize: 30))),
          // ),
        ],
      ),
    ),
  );
}

Future<void> setUsername(textUsername) async {
  final SharedPreferences pref = await SharedPreferences.getInstance();
  pref.setString('username', textUsername);
}

Future<void> setPassword(textPassword) async {
  final SharedPreferences pref = await SharedPreferences.getInstance();
  pref.setString('password', textPassword);
}

Future<void> setStatus(textstatus) async {
  final SharedPreferences pref = await SharedPreferences.getInstance();
  pref.setString('status', textstatus);
}

AppBar buildAppBar() {
  return AppBar(
    title: Transform.translate(
      offset: Offset(-14.0, 0.0),
      child: Container(
        child: Row(children: [
          Center(
            child: Container(
              margin: const EdgeInsets.fromLTRB(10, 0, 10, 0),
              width: 50,
              height: 50,
              decoration: BoxDecoration(
                image: DecorationImage(
                  fit: BoxFit.fill,
                  image: AssetImage("assets/images/buu_logo_eng.png"),
                ),
              ),
            ),
          ),
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                "มหาวิทยาลัยบูรพา",
                style: TextStyle(fontSize: 18, color: Colors.black),
              ),
              Text(
                "Burapha University",
                style: TextStyle(fontSize: 15, color: Colors.black),
              ),
            ],
          )
        ]),
      ),
    ),
    backgroundColor: Colors.yellow[600],
    elevation: 0.0,
  );
}
